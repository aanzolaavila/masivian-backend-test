# Masivian Backend Test - Roulette

**Author:** Alejandro Anzola

## Endpoints
Base endpoint: `/api/v1/roulettes`

Every endpoint (except **GET `/`**) requires the `userId` defined inside an HTTP header (named as `Authorization`).


### POST `/`
Creates a new Roulette with a unique id

### PUT `/{id}/open`
Sets an existing roulette as open for new bets

### PUT `/{id}/close`
Sets an existing roulette as finished (it cannot be opened again) and will not receive any more bets. Also, it will receive the bets results.

### PUT `/{id}/bets/color`
Adds a new bet based on color into the specified roulette, the request body should be a JSON structure as
```json
{
    "amount" : 1500.0,
    "color" : "RED"
}
```

### PUT `/{id}/bets/number`
Adds a new bet based on a number into the specified roulette, he request body should be a JSON structure as
```json
{
    "amount" : 1850.15,
    "number" : 17
}
```

### GET `/`
Returns the list of current Roulettes registered along with their state and bets.