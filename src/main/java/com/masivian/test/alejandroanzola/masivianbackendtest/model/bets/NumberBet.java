package com.masivian.test.alejandroanzola.masivianbackendtest.model.bets;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NonNull;

@Getter
public class NumberBet extends Bet {

	public static final int MIN_NUMBER = 0;
	public static final int MAX_NUMBER = 36;
	final int number;

	public NumberBet(@NonNull final BigDecimal amount, @NonNull String userId, @NonNull final Integer number) {

		super(amount, userId);

		if (MIN_NUMBER <= number && number <= MAX_NUMBER) {
			this.number = number;
		} else {
			throw new IllegalArgumentException(String.format("Number for bet must be between 0 and 36, got [%d]", number));
		}

	}

	@Override public boolean isWinner(final int winningNumber) {

		return this.number == winningNumber;
	}

	@Override public BigDecimal getPrizeAmount() {

		return this.getAmount().multiply(BigDecimal.valueOf(5));
	}
}
