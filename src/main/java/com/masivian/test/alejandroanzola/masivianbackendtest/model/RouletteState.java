package com.masivian.test.alejandroanzola.masivianbackendtest.model;

public enum RouletteState {
	OPEN,
	CLOSED,
	FINISHED
}
