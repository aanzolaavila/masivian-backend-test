package com.masivian.test.alejandroanzola.masivianbackendtest.services;

import java.util.List;
import java.util.UUID;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.BetsResult;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.Roulette;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.ColorBet;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.NumberBet;

public interface RouletteService {

	UUID createRoulette();

	String openRoulette(UUID rouletteId);

	String betOnRouletteByColor(UUID rouletteId, ColorBet bet);

	String betOnRouletteByNumber(UUID rouletteId, NumberBet bet);

	BetsResult closeRoulette(UUID rouletteId);

	List<Roulette> getRoulettes();

}
