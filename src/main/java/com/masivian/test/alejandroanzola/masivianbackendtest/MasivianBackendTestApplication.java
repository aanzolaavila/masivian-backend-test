package com.masivian.test.alejandroanzola.masivianbackendtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasivianBackendTestApplication {

	public static void main(String[] args) {

		SpringApplication.run(MasivianBackendTestApplication.class, args);
	}

}
