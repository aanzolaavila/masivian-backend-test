package com.masivian.test.alejandroanzola.masivianbackendtest.model.bets;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NonNull;

@Data
public abstract class Bet {

	public static final int MAX_AMOUNT = 10_000;
	final BigDecimal amount;
	final String userId;

	protected Bet(@NonNull final BigDecimal amount, @NonNull final String userId) {

		this.userId = userId;

		if (BigDecimal.ZERO.compareTo(amount) < 0
			&& amount.compareTo(BigDecimal.valueOf(MAX_AMOUNT)) <= 0) {
			this.amount = amount;
		} else {
			throw new IllegalArgumentException(String.format("Bet money can be at most $%d and positive, got [%s]", MAX_AMOUNT, amount));
		}
	}

	public abstract boolean isWinner(int winningNumber);

	public abstract BigDecimal getPrizeAmount();
}
