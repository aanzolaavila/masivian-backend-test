package com.masivian.test.alejandroanzola.masivianbackendtest.model;

import java.util.List;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.Bet;
import lombok.Data;

@Data
public class BetsResult {

	final int winningNumber;
	final List<Bet> winningBets;

}
