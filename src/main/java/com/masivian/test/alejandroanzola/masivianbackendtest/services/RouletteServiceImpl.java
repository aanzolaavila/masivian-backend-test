package com.masivian.test.alejandroanzola.masivianbackendtest.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.BetsResult;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.Roulette;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.RouletteState;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.ColorBet;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.NumberBet;
import com.masivian.test.alejandroanzola.masivianbackendtest.repositories.RouletteRepository;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;

@Service
public class RouletteServiceImpl implements RouletteService {

	public static final String OK_RESPONSE = "OK";

	final RouletteRepository rouletteRepository;

	public RouletteServiceImpl(final RouletteRepository rouletteRepository) {

		this.rouletteRepository = rouletteRepository;
	}

	@Override public UUID createRoulette() {

		Roulette newRoulette = new Roulette();
		newRoulette.setId(UUID.randomUUID());
		return rouletteRepository.save(newRoulette).getId();
	}

	private Roulette getRouletteById(final UUID rouletteId) {

		Optional<Roulette> rouletteOptional = rouletteRepository.findById(rouletteId);
		if (rouletteOptional.isPresent()) {
			return rouletteOptional.get();
		}

		throw new IllegalArgumentException(String.format("Roulette with id [%s] does not exist", rouletteId));
	}

	@Override public String openRoulette(final UUID rouletteId) {

		Roulette roulette = getRouletteById(rouletteId);
		roulette.openRoulette();
		rouletteRepository.save(roulette);

		return OK_RESPONSE;
	}

	@Override public String betOnRouletteByColor(final UUID rouletteId, final ColorBet bet) {

		Roulette roulette = getRouletteById(rouletteId);
		roulette.addBet(bet);
		rouletteRepository.save(roulette);

		return OK_RESPONSE;
	}

	@Override public String betOnRouletteByNumber(final UUID rouletteId, final NumberBet bet) {

		Roulette roulette = getRouletteById(rouletteId);
		roulette.addBet(bet);
		rouletteRepository.save(roulette);

		return OK_RESPONSE;
	}

	@Override public BetsResult closeRoulette(final UUID rouletteId) {

		Roulette roulette = getRouletteById(rouletteId);
		if (roulette.getState().equals(RouletteState.OPEN)) {
			roulette.closeRoulette(Roulette.generateWinningNumber());
			rouletteRepository.save(roulette);
		}

		return roulette.generateBetsResult();
	}

	@Override public List<Roulette> getRoulettes() {

		return IterableUtils.toList(rouletteRepository.findAll());
	}
}
