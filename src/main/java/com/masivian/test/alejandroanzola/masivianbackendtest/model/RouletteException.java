package com.masivian.test.alejandroanzola.masivianbackendtest.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RouletteException extends RuntimeException {

	public RouletteException() {

	}

	public RouletteException(final String message) {

		super(message);
	}

	public RouletteException(final String message, final Throwable cause) {

		super(message, cause);
	}

	public RouletteException(final Throwable cause) {

		super(cause);
	}

	public RouletteException(final String message, final Throwable cause, final boolean enableSuppression,
							 final boolean writableStackTrace) {

		super(message, cause, enableSuppression, writableStackTrace);
	}
}
