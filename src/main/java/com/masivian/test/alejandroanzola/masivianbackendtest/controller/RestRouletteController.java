package com.masivian.test.alejandroanzola.masivianbackendtest.controller;

import java.util.List;
import java.util.UUID;

import com.masivian.test.alejandroanzola.masivianbackendtest.controller.dtos.ColorBetDto;
import com.masivian.test.alejandroanzola.masivianbackendtest.controller.dtos.NumberBetDto;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.BetsResult;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.Roulette;
import com.masivian.test.alejandroanzola.masivianbackendtest.services.RouletteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/roulettes")
public class RestRouletteController implements RouletteController {

	final RouletteService rouletteService;

	public RestRouletteController(final RouletteService rouletteService) {

		this.rouletteService = rouletteService;
	}

	@PostMapping
	@Override public UUID createRoulette() {

		return rouletteService.createRoulette();
	}

	@PutMapping("{id}/open")
	@Override public String openRoulette(@PathVariable("id") final UUID rouletteId) {

		return rouletteService.openRoulette(rouletteId);
	}

	@PutMapping("{id}/bets/color")
	@Override public String betOnRouletteByColor(@PathVariable("id") final UUID rouletteId,
												 @RequestHeader("Authorization") final String userId,
												 @RequestBody final ColorBetDto bet) {

		return rouletteService.betOnRouletteByColor(rouletteId, bet.getBet(userId));
	}

	@PutMapping("{id}/bets/number")
	@Override public String betOnRouletteByNumber(@PathVariable("id") final UUID rouletteId,
												  @RequestHeader("Authorization") final String userId,
												  @RequestBody final NumberBetDto bet) {

		return rouletteService.betOnRouletteByNumber(rouletteId, bet.getBet(userId));
	}

	@PutMapping("{id}/close")
	@Override public BetsResult closeRoulette(@PathVariable("id") final UUID rouletteId) {

		return rouletteService.closeRoulette(rouletteId);
	}

	@GetMapping
	@Override public List<Roulette> getRoulettes() {

		return rouletteService.getRoulettes();
	}
}
