package com.masivian.test.alejandroanzola.masivianbackendtest.model.bets;

import java.math.BigDecimal;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.BetColor;
import lombok.Getter;
import lombok.NonNull;

@Getter
public class ColorBet extends Bet {

	final BetColor color;

	public ColorBet(@NonNull final BigDecimal amount, @NonNull String userId, @NonNull final BetColor color) {

		super(amount, userId);
		this.color = color;
	}

	@Override public boolean isWinner(final int winningNumber) {

		final boolean isEven = winningNumber % 2 == 0;

		return this.color == (isEven ? BetColor.RED : BetColor.BLACK);
	}

	@Override public BigDecimal getPrizeAmount() {

		return this.getAmount().multiply(BigDecimal.valueOf(1.8));
	}
}
