package com.masivian.test.alejandroanzola.masivianbackendtest.repositories;

import java.util.UUID;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.Roulette;
import org.springframework.data.repository.CrudRepository;

public interface RouletteRepository extends CrudRepository<Roulette, UUID> {

}
