package com.masivian.test.alejandroanzola.masivianbackendtest.controller;

import java.util.List;
import java.util.UUID;

import com.masivian.test.alejandroanzola.masivianbackendtest.controller.dtos.ColorBetDto;
import com.masivian.test.alejandroanzola.masivianbackendtest.controller.dtos.NumberBetDto;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.BetsResult;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.Roulette;

public interface RouletteController {

	UUID createRoulette();

	String openRoulette(UUID rouletteId);

	String betOnRouletteByColor(UUID rouletteId, String usedId, ColorBetDto bet);

	String betOnRouletteByNumber(UUID rouletteId, String usedId, NumberBetDto bet);

	BetsResult closeRoulette(UUID rouletteId);

	List<Roulette> getRoulettes();

}
