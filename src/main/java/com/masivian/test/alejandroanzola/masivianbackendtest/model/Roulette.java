package com.masivian.test.alejandroanzola.masivianbackendtest.model;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.Bet;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.Nullable;

@Getter
@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Roulette {

	public static final int MIN_NUMBER = 0;
	public static final int MAX_NUMBER = 36;

	@Id @Setter UUID id;
	@JsonIgnore @Version Long version;
	RouletteState state;
	List<Bet> bets;
	@Nullable Integer winningNumber;

	public Roulette() {

		bets = new ArrayList<>();
		state = RouletteState.CLOSED;
	}

	public void addBet(Bet bet) {

		if (this.state != RouletteState.OPEN) {
			throw new RouletteException("Roulette must be open to add a new bet");
		}
		bets.add(bet);
	}

	public void closeRoulette(final int winningNumber) {

		if (winningNumber < MIN_NUMBER || winningNumber > MAX_NUMBER) {
			throw new IllegalArgumentException(
					String.format("Roulette winning number must be between [%d] and [%d], got [%d]",
								  MIN_NUMBER, MAX_NUMBER, winningNumber));
		}

		if (this.state != RouletteState.OPEN) {
			throw new RouletteException("Roulette must be open if is going to be closed");
		}

		this.state = RouletteState.FINISHED;
		this.winningNumber = winningNumber;
	}

	public void openRoulette() {

		if (this.state != RouletteState.CLOSED) {
			throw new RouletteException("Roulette must be closed if it is going to be opened");
		}
		this.state = RouletteState.OPEN;
	}

	public BetsResult generateBetsResult() {

		if (this.state != RouletteState.FINISHED) {
			throw new RouletteException("Roulette must be finished if it is going to get prize");
		}

		Validate.notNull(this.winningNumber,
						 "Roulette is in an inconsistent state, if it is finished it should have a defined winning number");

		var winningBets = this.bets.stream()
								   .filter(x -> x.isWinner(this.winningNumber))
								   .collect(Collectors.toList());

		return new BetsResult(this.winningNumber, winningBets);
	}

	public static int generateWinningNumber() {

		Random random = new SecureRandom();
		return random.nextInt(MAX_NUMBER - MIN_NUMBER + 1) + MIN_NUMBER;
	}

}
