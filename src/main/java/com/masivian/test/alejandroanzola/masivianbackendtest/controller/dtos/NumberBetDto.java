package com.masivian.test.alejandroanzola.masivianbackendtest.controller.dtos;

import java.math.BigDecimal;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.NumberBet;
import lombok.Data;

@Data
public class NumberBetDto {

	BigDecimal amount;
	Integer number;

	public NumberBet getBet(final String userId) {
		return new NumberBet(this.amount, userId, this.number);
	}
}
