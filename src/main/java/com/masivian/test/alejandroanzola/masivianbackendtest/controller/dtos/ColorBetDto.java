package com.masivian.test.alejandroanzola.masivianbackendtest.controller.dtos;

import java.math.BigDecimal;

import com.masivian.test.alejandroanzola.masivianbackendtest.model.BetColor;
import com.masivian.test.alejandroanzola.masivianbackendtest.model.bets.ColorBet;
import lombok.Data;

@Data
public class ColorBetDto {

	BigDecimal amount;
	BetColor color;

	public ColorBet getBet(final String userId) {
		return new ColorBet(this.amount, userId, this.color);
	}
}
